#include <algorithm>
#include <iostream>

#include "observer/i_observer.hpp"
#include "observer/i_subject.hpp"

class MasterSubject : public ISubject
{
public:
    // Attach an observer to the subject
    void attach(IObserver* observer) override
    {
        observers.push_back(observer);
    }

    // Detach an observer from the subject
    void detach(IObserver* observer) override
    {
        observers.erase(std::remove(observers.begin(), observers.end(), observer), observers.end());
    }

    // Notify all observers about an event
    void notify() override
    {
        for (auto& observer : observers)
        {
            observer->update();
        }
    }
    private:
    // List of observers
    std::vector<IObserver*> observers;
};

MasterSubject masterSubject;

class Observer : public IObserver
{
public:
    // Update method is called by the subject
    void update() override
    {
        std::cout << "Observer is notified." << std::endl;
    }
};

int main()
{
    Observer observerOfMasterSubject;

    masterSubject.attach(&observerOfMasterSubject);

    masterSubject.notify();

    return 0;
}