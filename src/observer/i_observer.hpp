#pragma once

class IObserver
{
public:
    // Virtual destructor is needed to avoid memory leak
    virtual ~IObserver() = default;

    // Update method is called by the subject
    virtual void update() = 0;
};