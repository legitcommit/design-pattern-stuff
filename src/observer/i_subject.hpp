#pragma once

#include "i_observer.hpp"

#include <vector>

class ISubject
{
public:
    // Virtual destructor is needed to avoid memory leak
    virtual ~ISubject() = default;

    // Attach an observer to the subject
    virtual void attach(IObserver* observer) = 0;

    // Detach an observer from the subject
    virtual void detach(IObserver* observer) = 0;

    // Notify all observers about an event
    virtual void notify() = 0;
};